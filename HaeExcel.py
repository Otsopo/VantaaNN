#TARKISTA, ETTÄ Vantaan_tulevaisuuskysely.xlsx ON SAMASSA KANSIOSSA.


from openpyxl import load_workbook

#The workbook and the worksheet that are used.
#If used for neural networks the input and output values should be grouped.
workbook = load_workbook('Vantaan_tulevaisuuskysely.xlsx')
worksheet = workbook.active

#In case the first column is used as a name, use this method for importing them.
def getAttributeNamesFromExcel():
    attributeNames = []
    row = 1
    column = 1
    cell = worksheet.cell(row=row, column=column)
    cellValue = cell.value
    while( cellValue != None or str(cellValue) != "None"):
        
        attributeNames.append(cellValue)
        column+=1
        cell = worksheet.cell(row=row, column=column)
        cellValue = cell.value
    return attributeNames

#Imports all the values from the given excel worksheet
def getAttributesFromExcel():
    attributeArrays = []
    attributes = []
    row = 2
    column = 1
    cell = worksheet.cell(row=row, column=column)
    cellValue = cell.value
    
    while(cellValue != None or str(cellValue) != "None"):
        
        while( cellValue != None or str(cellValue) != "None"):
            attributes.append(cellValue)
            column+=1
            cell = worksheet.cell(row=row, column=column)
            cellValue = cell.value
        attributeArrays.append(attributes)
        attributes = []
        column = 1
        row += 1
        cell = worksheet.cell(row=row, column=column)
        cellValue = cell.value
    return attributeArrays

#Splits the data to Train and Test data. The split is hardcoded at the moment.
#Parameter has to be the form [[inputs],[outputs]]
def splitTrainDataToTest(inputsAndOutputs):
    trainInputsAndOutputs = []
    testInputsAndOutputs = []
    trainAndTestData = []
    trainInputs = []
    testInputs = []
    trainOutputs = []
    testOutputs = []
    split = 561
    
    trainInputs = inputsAndOutputs[0][0:split]
    testInputs = inputsAndOutputs[0][split:850]
    trainOutputs = inputsAndOutputs[1][0:split]
    testOutputs = inputsAndOutputs[1][split:850]
    
    trainInputsAndOutputs.append(trainInputs)
    trainInputsAndOutputs.append(trainOutputs)
    testInputsAndOutputs.append(testInputs)
    testInputsAndOutputs.append(testOutputs)
    trainAndTestData.append(trainInputsAndOutputs)
    trainAndTestData.append(testInputsAndOutputs)
    
    return trainAndTestData

#Separates attributes to inputs and outputs.
def divideAttributes(attributeArrays):
    inputs = []
    outputs = []
    inputsAndOutputs = []
    
    for attributes in attributeArrays:
        #6 and 52 are hardcoded values that express the border between inputs
        #and outputs in the list.
        inputs.append(attributes[6:52])
        outputs.append(attributes[0:6])
    
    inputsAndOutputs.append(inputs)
    inputsAndOutputs.append(outputs)
    return inputsAndOutputs

#Prints the excel's values
def readExcel():
    attributeArrays = getAttributesFromExcel()
    inputsAndOutputs = divideAttributes(attributeArrays)
    inputs = inputsAndOutputs[0]
    outputs = inputsAndOutputs[1]
    dataLimit = 500;
    if(len(inputs) > dataLimit or len(outputs) > dataLimit):
        print('Your data is too big to be printed')
        print('Inputs lenght (must be under '+str(dataLimit)+'):', len(inputs));
        print('Outputs lenght (must be under '+str(dataLimit)+'):', len(outputs));

    else:
        print('inputs',inputs)
        print('outputs',outputs)            

    

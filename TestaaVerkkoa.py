#Aja tämä tiedosto vasta neuroverkon suorittamisen jälkeen niin,
#että sinulla on VantaaSyn.pkl tiedosto!

#Monivalintakysymyksiin vastataan muodossa 0,0,1,1,0 , jokaiseen
#vastausvaihtoehtoon on laitettava 0 tai 1, yhteensä niitä tulee olla aina
#yhtä monta kuin vastausvaihtoehtoja.

#Esimerkiksi jos vastaukset ovat:
#0,1,1,0,0,0,1
#1,0,1,0,1,0,1,0,0
#1,1,0,0,0,1,0
#1,1,0,1,0,1,0,0
#1,0,1,1,1,1,0
#1,1,1,1,0,1,0,0
#
#Niin tulokset ovat muotoa [ 1.361  2.824  1.981  9.051  2.431  6.887].
#Tulkitse ne joko Vantaan_tulevaisuuskysely.xlsx toiselta välilehdeltä,
#tai CRIPS -raportin Datan kuvaus -osiosta.

#Arvot ovat järjestyksessä: Millainen tulevaisuudenajattelija olet?, Ikä, Sukupuoli,
#Asuinpaikka, Perhetilanne ja Koulutus.
#Eli vastuksen perusteella:

#Millainen tulevaisuuden ajattelija olet: Tulevaisuuden visionääri
#Ikä: 25-34
#Sukupuoli: Mies
#Asuinpaikka: Muu
#Keitä kotonasi asuu: Asun avopuolison kanssa
#Koulutuksesi: Korkeakoulu, alemman asteen tutkinto

import pickle
import numpy as np
np.set_printoptions(precision=3)
np.set_printoptions(threshold=np.nan)
inputs = []
questions = ['Asun Vantaalla\n'+
             'Kouluni sijaitsee Vantaalla\n' +
             'Työpaikkani sijaitsee Vantaalla\n'+
             'Olen Vantaalainen yrittäjä\n'+
             'Olen töissä Vantaan kaupungin organisaatiossa\n'+
             'Olen poliittinen toimija Vantaalla\n'+
             'Olen muusta syystä kiinnostunut vantaasta\n',
             
             'Työn merkitystä ja hyödyllisyyttä\n'+
             'Työn joustavuutta\n'+
             'Työstä maksettavaa palkkaa\n'+
             'Työn säännöllisyyttä ja vakautta\n'+
             'Työn yhteisöllisyyttä\n'+
             'Työn itsenäisyyttä\n'+
             'Työn luovuutta\n'+
             'Työssä kehittymistä ja uuden oppimista\n'+
             'Muuta',
             
             'Pelkkään siirtymiseen\n'+
             'Omalla lihasvoimalla liikkumiseen (esim.pyörä)\n'+
             'Nukkumiseen\n'+
             'Vuorovaikutukseen\n'+
             'Lukemiseen tai viihteen kuluttamiseen\n'+
             'Rentoutumiseen\n'+
             'Jotenkin muuten',
             
             'Aikataulujen puuttumista\n'+
             'Sosiaalisia suhteita\n'+
             'Aikaa perheen kanssa\n'+
             'Mielekästä tekemistä ja harrastuksia\n'+
             'Itsensä kehittämistä\n'+
             'Lepoa ja latautumista\n'+
             'Uusia elämyksiä\n'+
             'Muuta',
             
             'Kodin lähialuleilla\n'+
             'Vantaalla\n'+
             'Muuaalla pääkaupunkiseudulla\n'+
             'Kauempana suomessa\n'+
             'Ulkomailla\n'+
             'Internetin sosiaalisessa verkostossa\n'+
             'Muualla\n',
             
             'Ihmisten tuttuutta\n'+
             'Yhteisiä mielenkiinnon kohteita\n'+
             'Yhteisiä arvoja\n'+
             'Kasvokkain tapahtuvia kohtaamisia\n'+
             'Laajan ihmisjoukon tavoittamista\n'+
             'Yhteenkuuluvuuden ja pysyvyyden tunnetta\n'+
             'Monipuolisia ja muuttuvia verkostoja\n'+
             'Muuta']

def getInput(question,i):
    global questions
    global inputs
    print('\n---\n'+question+'\n---\n')
    print(questions[i])
    ins = input('')
    for inpu in ins.split(','):
        inputs.append(int(inpu))
        
def nonlin(x,deriv=False):

        if(deriv==True):
                derSig = x*(1-x)
                return derSig

        sig = 1.0/(1.0+np.exp(-x))
        return sig
    
getInput('Miten Vantaa liittyy sinuun?',0)
getInput('Arvostan työnteossa eniten',1)
getInput('Miten haluaisit käyttää päivittäisiin matkoihin kuluvan ajan 10 vuoden kuluttua?',2)
getInput('Arvostan vapaa-ajassa eniten',3)
getInput('Missä haluaisit tavata tuttaviasi ja ystäviäsi 10 vuoden kuluttua?',4)
getInput('Arvostan sosiaalisissa verkostoissa eniten',5)


with open('VantaaSyn.pkl','rb') as f:  # Python 3: open(..., 'rb')
                syn1, syn0 = pickle.load(f)
bias = 5
inputs.append(bias)               
X = np.array(inputs)
l0 = X
l1 = nonlin(np.dot(l0,syn0))
l2 = nonlin(np.dot(l1,syn1))
print(l2*10)

with open('VantaaSyn.pkl', 'wb') as f:  # Python 3: open(..., 'wb')
    pickle.dump([syn1, syn0], f)

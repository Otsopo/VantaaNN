#Verkon pohjana käytettiin osoitteesta https://iamtrask.github.io/2015/07/12/basic-python-network/ löytyvää mallia.
#Kokeilin lisätä ylimääräisen hidden layerin, muttei se parantanut tulosta.

import numpy as np
import pickle
import itertools
import HaeExcel as attrs

#nonlin contains sigmoid function and its derivate function
def nonlin(x,deriv=False):

        if(deriv==True):
                derSig = x*(1-x)
                return derSig

        sig = 1.0/(1.0+np.exp(-x))
        return sig

#Validation method  for 66% split validation
def splitValidate66(testData):
        global bias
        allPredictions = 0
        rightPredictions = 0
        #Counts the right predictions per attribute
        rightByAttribute = [0,0,0,0,0,0]
        
        testInputs = testData[0]
        for input in testInputs:
            if(len(input)==46):
                input.append(bias)
        testOutputs = testData[1]
        
        l0 = np.array(testInputs)
        l1 = nonlin(np.dot(l0,syn0))
        l2 = nonlin(np.dot(l1,syn1))
        l2 = l2*10;
        
        for i in range(0,len(l2)):
                outputRow = l2[i]
                testRow = testOutputs[i]
                
                for j in range(0,len(outputRow)):
                        if(int(outputRow[j] + 0.5)==testRow[j]):
                                rightByAttribute[j] += 1
                                rightPredictions += 1
                                allPredictions += 1
                        else:
                                allPredictions += 1
                                
        print('Oikein per kysymys:')
        print('Millainen tulevaisuuden ajattelija olet?: ', rightByAttribute[0])
        print('Ikäsi?: ', rightByAttribute[1])
        print('Sukupuolesi?: ', rightByAttribute[2])
        print('Missä asut tällä hetkellä?: ', rightByAttribute[3])
        print('Keitä kotonasi asuu tällä hetkellä?: ', rightByAttribute[4])
        print('Korkein suoritettu tutkinto?: ', rightByAttribute[5],'\n')
        print('Oikein olevia: ' + str(rightPredictions) + '/' + str(allPredictions)+ '\n')




        

#Initialize variables
#Weights
syn1 = 0
syn0 = 0
#Learning rate and bias
LRate = 0.03
bias = 5

#Numpy print options
np.set_printoptions(precision=3)
np.set_printoptions(threshold=np.nan)

#Get values from excel
attributeArrays = attrs.getAttributesFromExcel();

#Split them to inputs and outputs
inputsAndOutputs = attrs.divideAttributes(attributeArrays)

#Split the data to train and test data
trainAndTestData = attrs.splitTrainDataToTest(inputsAndOutputs)

trainData = trainAndTestData[0]
trainInputs = trainData[0]
trainOutputs = trainData[1]
testData = trainAndTestData[1]

#Unnecessary operation, but too annoying to refactor using the basic editor.
inputs = trainInputs
outputs = trainOutputs

#Add bias to inputs
for input in inputs:
    input.append(bias)
    x = 0
    for attribute in input:
        input[x] = attribute
        x += 1
        
#Divide outputs by 10 so the output values are between 0 and 1
for output in outputs:
        x = 0
        for attribute in output:
                attribute = attribute/10
                output[x] = attribute
                x += 1
                
X = np.array(inputs)
y = np.array(outputs)
np.random.seed(2)

#Layer sizes
l0Size = len(inputs[0]) 
l1Size = 40
l2Size = len(outputs[0])

#Try to import old Syn values from a file            
try:
        # Getting back the weights:
        with open('VantaaSyn.pkl','rb') as f:  # Python 3: open(..., 'rb')
                syn1, syn0 = pickle.load(f)
except EOFError:
        syn0 = 2*np.random.random((l0Size,l1Size)) - 1
        syn1 = 2*np.random.random((l1Size,l2Size)) - 1
except FileNotFoundError:
        syn0 = 2*np.random.random((l0Size,l1Size)) - 1
        syn1 = 2*np.random.random((l1Size,l2Size)) - 1

#The learning process!
for j in range(1750):

        #Layers
        l0 = X
        l1 = nonlin(np.dot(l0,syn0))
        l2 = nonlin(np.dot(l1,syn1))

        # How much did we miss?
        l2_error = (y - l2)
    
        if (j% 250) == 0:
                splitValidate66(testData)
        
        # In what direction is the target value?
        l2_delta = l2_error*nonlin(l2,deriv=True)
        # How much did each l1 value contribute to the l2 error (according to the weights)?
        l1_error = l2_delta.dot(syn1.T)
        # In what direction is the target l1?
        l1_delta = l1_error * nonlin(l1,deriv=True)

        syn1 += l1.T.dot(l2_delta * LRate)
        syn0 += l0.T.dot(l1_delta * LRate)
    
with open('VantaaSyn.pkl', 'wb') as f:  # Python 3: open(..., 'wb')
        pickle.dump([syn1, syn0], f)
    
splitValidate66(testData);


        
#Extra layer stuff

#lx_error = l2_delta.dot(synx.T)
#lx_delta = lx_error * nonlin(lx,deriv=True)
#l1_error = lx_delta.dot(syn1.T)
# in what direction is the target l1?
#syn1 += l1.T.dot(lx_delta * LRate)
#synx += lx.T.dot(l2_delta * LRate)
#lx = nonlin(np.dot(l1,syn1))
#l2 = nonlin(np.dot(lx,synx))
#syn1 = 2*np.random.random((l1Size,lxSize)) - 1
#synx = 2*np.random.random((lxSize,l2Size)) - 1        
        
